# image-compression-sampler

This is just a script that tries to show off differing image compression
outcomes.

Run with `--help` to see the few options you have to play with.